const { add } = require("./string_calculator");

test("Add numbers function", () => {

    expect(add("")).toBe(0);

    expect(add("1")).toBe(1);

    expect(add("1,2")).toBe(3);

    expect(add("1\n2,3")).toBe(6);

    // expect(add("//;\n1;2")).toBe(3);

    expect(() => add("-5")).toThrow();

    expect(add("1,1100")).toBe(1);

    // expect(add("//[***]\n1***2***3")).toBe(6);

    // expect(add("//[*][%]\n1*2%3")).toBe(6);

    // expect(add("//[**][%%]\n1**2%%3")).toBe(6);
});

