class StringCalculator {
    Add(stringNum) {
        let sum = 0;
        let strArr = stringNum.split(/[^-0-9]/g);
        let negativeNum = strArr.filter((val) => Number(val) < 0);
        let bigNum = strArr.filter((val) => Number(val) >= 1000);

        if (strArr != "") {
            if (negativeNum.length > 0) {
                throw new Error("no negatives allowed: " + negativeNum.join(","));
            }
            if (bigNum.length > 0) {
                sum = strArr.reduce((previous, current) => {
                    return (parseInt(previous) + parseInt(current)) - bigNum
                });
            }

            else {
                sum = strArr.reduce((previous, current) => {
                    return parseInt(previous) + parseInt(current)
                });
            }
        }
        else {
            sum = 0;
        }

        return parseInt(sum)

    }
}

const castringCalc = new StringCalculator();

module.exports = {
    add: castringCalc.Add,
};



