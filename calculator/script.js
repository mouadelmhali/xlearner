let Numbuttons = Array.from(document.getElementsByClassName("Numbutton"));
let functionButtons = Array.from(
    document.getElementsByClassName("functionButton")
);
let display = document.getElementById("display");
let result = "";
Numbuttons.map((Numbutton) => {
    Numbutton.addEventListener("click", (e) => {
        display.innerText += e.target.innerText;
        result += e.target.innerText;
    });
});
functionButtons.map((functionButton) => {
    functionButton.addEventListener("click", (e) => {
        switch (e.target.innerText) {
            case 'C':
                display.innerText = "";
                break
            case "←":
                display.innerText = (display.innerText).slice(0, -1);
                break
            case "/":
                display.innerText = "";
                result += "/";
                break
            case "*":
                display.innerText = "";
                result += "*";
                break
            case "-":
                display.innerText = "";
                result += "-";
                break
            case "+":
                display.innerText = "";
                result += "+";
                break
            case "=":
                display.innerText = eval(result);
                break
        }
    });
});
